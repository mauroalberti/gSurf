# gSurf

This is the development version, named "profiles", of the gSurf tool.
This version is devoted inserting tools for the structural analysis
of geological surfaces, starting from the simulation of geological 
surfaces, the calculation of the intersections between a geological
surface and a topographic surface (expressed by a DEM), the restoration
of a geological surface from its traces on a DEM.

Also the tools already available in the QGIS plug-in qgSurf are planned 
to be inserted.


### Requirements
It depends on the **dev** version of pygsf and gst, both available at:
https://gitlab.com/mauroalberti
  
Afterwards, from the folder where the gSurf_profiles.py file is located, launch the tool as a Python 3 module:
 ```bash
python -m gSurf
 ```

### Status

Currently, it is in an alpha stage.
The branch was started 2019-09-29 but had a long period of inactivity.
It is revitalized starting 2022-11-28. 



